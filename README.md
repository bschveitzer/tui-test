# Tui-test

### Considerações gerais
A tarefa em si é relativamente grande, com os dias corridos, tentei focar na função primária que é a parte de busca de ofertas de hotéis. Gastei cerca de 12 horas no total. Com mais tempo dedicado, várias coisas poderiam ser melhoradas ou até mesmo implementadas. O projeto foi feito criado pelo vue-cli, já com Vue 3 e dependências atualizadas. Foi utilizado a API do Amadeus para fazer a busca de cidades e de hotéis, como estava usando uma versão de teste, dentro daqueles países da descrição, apenas Espanha está disponível. A parte superior serve para navegação entre as páginas. Na página Home está o fomrulário de pesquisa, onde o usuário pode buscar pelas cidades usando auto-complete, escolher o range de datas e o número de hóspedes. Ao clicar em pesquisa é redirecinado para a página de Explorar onde em teoria, haveriam os resultados da pesquisa. Hoje os resultados estão apenas no VueX, já funcionais. Nesse ponto, foi utilizado o VueX para gerenciamento de estados e vue-router para navegação.

### Pontos de melhorias
Listo aqui alguns pontos que identifiquei e que melhoraria:  
-> Melhoria geral no visual. Tentei seguir o máximo o TAGUS, porém algumas coisas estão fora do padrão e outras com o visual não tanto como gostaria, digamos que falta o refinamento.    
-> Página de resultados, gostaria de ter feito o card de hoteis e montado a página de resultados corretamente, seguindo a ideia do stay.tui.com  
-> Testes, apenas montei a estrutura de testes e deixei os arquivos da maneira que imagino uma boa estrutura de testes, tanto unitário quanto e2e.  
-> i18n, hoje só existe os locales em português, seria interessante adicionar em outras linguagens.  
-> Animações de feedback e loading, gostaria de ter implementado essas animações para uma melhor experiência do usuário.  
-> Utilização do moment.js ou outra ferramenta de datas, simplificaria bastante o código em certos pontos.  
-> Tirar API keys do .env, é inseguro, porém sem um backend dedicado fica impossível.  
-> LocalForage para guardar as informações de pesquisa em cache, para quando o reload da página acontecesse, não perdesse as informações.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```