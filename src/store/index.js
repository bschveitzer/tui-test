import { createStore } from 'vuex';
import search from './search.store';

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    search,
  },
});
