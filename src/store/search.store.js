import { handleCityByName, handleSearch } from '@/services/search.api';

export default {
  state: () => ({
    searchTerm: '',
    foundCities: [],
    searchInfo: {
      selected: null,
      checkInDate: new Date().setDate(new Date().getDate() + 1),
      checkOutDate: new Date().setDate(new Date().getDate() + 2),
      adults: 2,
    },
    payloadInfo: {
      selected: 'BCN',
      checkInDate: '2021-12-19',
      checkOutDate: '2021-12-25',
      adults: 2,
    },
    foundHotels: [],
  }),
  getters: {
    citiesByCountry: state => state.foundCities.filter(city => city.address.countryCode === 'ES' || city.address.countryCode === 'BR'),
  },
  mutations: {
    saveSearchTerm(state, payload) {
      state.searchTerm = payload;
    },
    setFoundCities(state, payload) {
      state.foundCities = payload;
    },
    setSearchInfo(state, payload) {
      state.searchInfo = payload;
    },
    setFoundHotels(state, payload) {
      state.foundHotels = payload;
    },
    setPayloadInfo(state, payload) {
      state.payloadInfo = payload;
    },
  },
  actions: {
    searchCities({ commit }, { searchTerm }) {
      commit('saveSearchTerm', searchTerm);
      handleCityByName(searchTerm).then(cities => commit('setFoundCities', cities));
    },
    clearSearch({ commit }) {
      commit('saveSearchTerm', '');
      commit('setFoundCities', []);
    },
    prepareData({ commit }, payload) {
      commit('setSearchInfo', payload);
      const startMonth = payload.checkInDate.getMonth() + 1 < 10 ? '0' + (payload.checkInDate.getMonth() + 1) : payload.checkInDate.getMonth() + 1;
      const endMonth = payload.checkOutDate.getMonth() + 1 < 10 ? '0' + (payload.checkOutDate.getMonth() + 1) : payload.checkOutDate.getMonth() + 1;

      const formattedStartDate = `${payload.checkInDate.getFullYear()}-${startMonth}-${payload.checkInDate.getDate()}`;
      const formattedEndDate = `${payload.checkOutDate.getFullYear()}-${endMonth}-${payload.checkOutDate.getDate()}`;

      payload.checkInDate = formattedStartDate;
      payload.checkOutDate = formattedEndDate;
      commit('setPayloadInfo', payload);
    },
    searchHotels({ commit, state }) {
      handleSearch(state.payloadInfo).then(hotels => commit('setFoundHotels', hotels));
      return state.foundHotels;
    },
  },
};
