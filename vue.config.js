module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/style/_variables.sass"
          @import "@/style/_breakpoints.sass"
        `,
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'pt-br',
      fallbackLocale: 'pt-br',
      localeDir: 'locales',
      enableLegacy: true,
      runtimeOnly: false,
      compositionOnly: true,
      fullInstall: true,
    },
  },
};
