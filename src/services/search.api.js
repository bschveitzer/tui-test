import { AmadeusApiV1, AmadeusApiV2 } from './api';

export function handleCityByName(name) {
  return AmadeusApiV1.get(`/reference-data/locations?subType=CITY&keyword=${name}`)
    .then((response) => response.data.data)
    .catch((err) => {
      console.log('err', err);
    });
}

export function handleSearch(payload) {
  return AmadeusApiV2.get(`/shopping/hotel-offers?cityCode=${payload.selected}&roomQuantity=1&adults=${payload.adults}&checkInDate=${payload.checkInDate}&checkOutDate=${payload.checkOutDate}&radius=5&radiusUnit=KM&paymentPolicy=NONE&includeClosed=false&bestRateOnly=true&view=FULL&sort=NONE`)
    .then((response) => response.data.data)
    .catch((err) => {
      console.log('err', err);
    });
}
