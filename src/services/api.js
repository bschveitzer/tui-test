import axios from 'axios';

export const AmadeusApiV1 = axios.create({
  baseURL: 'https://test.api.amadeus.com/v1',
  timeout: 20000,
});

export const AmadeusApiV2 = axios.create({
  baseURL: 'https://test.api.amadeus.com/v2',
  timeout: 20000,
});

export function getAmadeusAccessToken() {
  const params = new URLSearchParams();
  params.append('grant_type', 'client_credentials');
  params.append('client_id', process.env.VUE_APP_AMADEUS_API_KEY);
  params.append('client_secret', process.env.VUE_APP_AMADEUS_SECRET_KEY);

  AmadeusApiV1.post('/security/oauth2/token', params)
    .then((response) => {
      AmadeusApiV1.defaults.headers.common.Authorization = `Bearer ${response.data.access_token}`;
      AmadeusApiV2.defaults.headers.common.Authorization = `Bearer ${response.data.access_token}`;
    })
    .catch((err) => {
      console.log('err', err);
    });
}
